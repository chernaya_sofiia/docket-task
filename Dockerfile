FROM node:12-alpine

COPY public ./public
COPY src ./src
COPY .babelrc ./
COPY .eslintrc ./
COPY config.js ./
COPY package.json ./
COPY server.js ./
RUN npm install

CMD ["npm", "start"]
EXPOSE $PORT