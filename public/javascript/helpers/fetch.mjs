export const fetchText = async id => {
  const text = await fetch(`/game/texts/${id}`);
  return text.json();
};
