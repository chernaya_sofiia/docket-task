/* eslint-disable */
import { addClass, removeClass } from '../helpers/helper.mjs';
import { 
  addText,
  gameStartedView,
  gameEndedView } from '../roomView.mjs';
import { 
  hideButtons,
  showCountdownStart,
  setInnerText,
  showCommentator
} from '../helpers/roomViewHelper.mjs'
import { fetchText } from '../helpers/fetch.mjs';
/* eslint-enable */

let gameTextArray;
let currentCharacter = 0;
let totalCharacters = 0;
let currentRoom;
let userSocket;

const resetGame = () => {
  currentCharacter = 0;
  totalCharacters = 0;
  gameTextArray = [];
};

export const startCountdown = async ({ textId, roomId }, socket) => {
  userSocket = socket;
  currentRoom = roomId;
  hideButtons();
  showCountdownStart();
  const text = await fetchText(textId);
  addText(text);
};

const trackCharacter = event => {
  if (event.location !== 0) {
    return;
  }
  const currentSpan = gameTextArray[currentCharacter];

  if (event.key === currentSpan.innerText) {
    addClass(currentSpan, 'correct');
    currentCharacter += 1;
    removeClass(currentSpan, 'current-character');

    const progress = { currentCharacter, totalCharacters };
    userSocket.emit('GAME_PROGRESS',
      { progress,
        roomId: currentRoom
      });
    const nextSpan = gameTextArray[currentCharacter];
    if (!nextSpan) {
      document.removeEventListener('keydown', trackCharacter);
    } else {
      addClass(nextSpan, 'current-character');
    }
  }
};

export const startGame = time => {
  if (time > 0) {
    setInnerText('countdown-start', time);
    return;
  }
  resetGame();
  gameStartedView();
  gameTextArray = document.querySelectorAll('#game-text span');
  totalCharacters = gameTextArray.length;
  const currentSpan = gameTextArray[currentCharacter];
  addClass(currentSpan, 'current-character');
  document.addEventListener('keydown', trackCharacter);
};

export const stopGame = () => {
  document.removeEventListener('keydown', trackCharacter);
  gameEndedView();
};

export const updateCountdownEnd = time => {
  setInnerText('countdown-end', time);
};

export const updateCommentatorQuote = quote => {
  setInnerText('commentator-quote', quote);
  showCommentator();
};
