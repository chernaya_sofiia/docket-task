export const quotes = {
  typesOfPhrases: ['startQuote', 'transport', 'jokes', 'places', 'finishQuote'],
  startQuote: 'На улице сейчас немного пасмурно, но на Львов Арена сейчас \
просто замечательная атмосфера: двигатели рычат, зрители улыбаются а гонщики \
едва заметно нервничают и готовят своих железных коней к заезду. А комментировать \
всё это действо буду я, Эскейп Энтерович и я рад вас приветствовать со словами \
Доброго Вам дня, господа!',
  finishQuote: ['Последним завершает круг '],
  transport: ['Tesla', 'BMW', 'Tayota', 'Mercedes', 'Audi'],
  beforeParticipans: 'Сегодня в гонке учавствуют: ',
  resultsOfRace: ['В гонке лидирует ', ', за ним ', '. Замыкает тройку лидеров'],
  jokes: ['joke1', 'joke2', 'joke3'],
  noOneWon: 'Никто не добрался до финиша.',
  onlyOneFinished: 'До финиша добрался только ',
  withResult: ' с результатом ',
  places: ['Первое место занимает ', ', на втором месте ', ', третье место досталось '],
  timeIsOver: 'Время вышло. ',
  finalResult: '. И финальный результат:\n',
  beforeFinisher: 'Финишную прямую пересёк ',
  nearFinish: 'приближается к финишу.',
  typesOfJokes: ['Classic', 'Programming', 'Ironic'],
  classicJokes: ['Classic joke 1', 'Classic joke 2', 'Classic joke 3'],
  programmingJokes: ['Programming joke 1', 'Programming joke 2', 'Programming joke 3'],
  ironicJokes: ['Ironic joke 1', 'Ironic joke 2', 'Ironic joke 3']
};

export default { quotes };
