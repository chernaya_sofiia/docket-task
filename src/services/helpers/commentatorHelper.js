import { random } from 'lodash';
import { SECONDS_TIMER_BEFORE_START_GAME, COMMENTATOR_NOTIFY_CHARACTERS_LEFT_TO_FINISH } from '../../config';
import { quotes } from '../../database/quotes';

export const getUsernameWithTransport = username => {
  const randomIndex = random(0, (quotes.transport.length - 1));
  return `${username} на ${quotes.transport[randomIndex]}`;
};

export const getParticipantsQuote = users => {
  const usersOnTransport = users.map((user => getUsernameWithTransport(user.username)));
  return usersOnTransport.reduce((accumulator, value) => `${accumulator} ${value}.\n`, quotes.beforeParticipans);
};

export const sortUsersByProgress = users => users
  .concat()
  .sort((a, b) => b.progress.currentCharacter - a.progress.currentCharacter);

export const getSortedByProgressUsernames = users => {
  const sorted = sortUsersByProgress(users);
  return sorted.map(user => user.username);
};

export const mapQuotesToUsernames = (quotes, usernames) => usernames.map(
  (username, index) => `${quotes[index]} ${username}`
);

export const getUsersProgressQuote = users => {
  const topThreeUsers = getSortedByProgressUsernames(users).slice(0, 3);
  const usersWithQuotes = mapQuotesToUsernames(quotes.resultsOfRace, topThreeUsers);
  return usersWithQuotes.join('');
};

export const secondsSpentForRace = (userFinished, gameStarted) => Math.round(
  ((userFinished - gameStarted) / 1000) - SECONDS_TIMER_BEFORE_START_GAME
);

export const isUserNearFinish = (currentCharacter, totalCharacters) => (
  (totalCharacters - currentCharacter) === COMMENTATOR_NOTIFY_CHARACTERS_LEFT_TO_FINISH
);

export class CommentatorPhrase {
  constructor(type) {
    this.type = type;
  }

  getQuote() {
    let quote;
    if (this.type === 'startQuote') {
      quote = quotes.startQuote;
    } else if (this.type === 'finishQuote') {
      quote = quotes.finishQuote;
    } else if (this.type === 'noOneWon') {
      quote = quotes.noOneWon;
    } else if (this.type === 'beforeFinisher') {
      quote = quotes.beforeFinisher;
    } else if (this.type === 'nearFinish') {
      quote = quotes.nearFinish;
    } else if (this.type === 'withResult') {
      quote = quotes.withResult;
    } else if (this.type === 'onlyOneFinished') {
      quote = quotes.onlyOneFinished;
    } else if (this.type === 'places') {
      quote = quotes.places;
    } else if (this.type === 'timeIsOver') {
      quote = quotes.timeIsOver;
    } else if (this.type === 'finalResult') {
      quote = quotes.finalResult;
    }
    return quote;
  }
}
