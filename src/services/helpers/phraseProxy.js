import { quotes } from '../../database/quotes';
import { CommentatorPhrase } from './commentatorHelper';

// proxy
export class PhraseProxy {
  constructor(type) {
    if (Object.values(quotes.typesOfPhrases).includes(type)) {
      this.phrase = new CommentatorPhrase(type);
    } else {
      console.error('No such phrase type');
    }
  }

  getText() {
    if (this.phrase) {
      return this.phrase.getQuote();
    }
    console.error('No phrase');
  }
}
