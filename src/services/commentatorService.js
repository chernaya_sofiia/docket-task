import { quotes } from '../database/quotes';
import roomDatabase from '../database/rooms';
import {
  getParticipantsQuote,
  secondsSpentForRace,
  getUsersProgressQuote,
  isUserNearFinish
} from './helpers/commentatorHelper';
import {
  SECONDS_SCHEDULED_ANNOUNCE_INTERVAL,
  SECONDS_SCHEDULED_JOKE_INTERVAL
} from '../config';
import { PhraseProxy } from './helpers/phraseProxy';

const activeCommentator = new Map();

const addCommentator = (roomId, commentator) => activeCommentator.set(roomId, commentator);
const getActiveCommentator = roomId => activeCommentator.get(roomId);

const deleteCommentator = roomId => {
  const commentator = activeCommentator.get(roomId);

  if (commentator) {
    const { scheduledAnnounceInterval, jokesInterval } = commentator;
    clearInterval(scheduledAnnounceInterval);
    clearInterval(jokesInterval);
    activeCommentator.delete(roomId);
  }
};

const updateScheduledAnnounceTime = roomId => {
  const commentator = activeCommentator.get(roomId);
  addCommentator(roomId, {
    ...commentator,
    previousAnnounce: Date.now(),
    nextScheduledAnnounceTime: (Date.now() + (SECONDS_SCHEDULED_ANNOUNCE_INTERVAL * 1000))
  });
};

const updatePreviousAnnounceTime = roomId => {
  const commentator = getActiveCommentator(roomId);
  addCommentator(roomId, {
    ...commentator,
    previousAnnounce: Date.now()
  });
};

const sendToRoom = (roomId, commentString) => {
  const { socket } = getActiveCommentator(roomId);
  socket.to(roomId).emit('COMMENTATOR_QUOTE', commentString);
  socket.emit('COMMENTATOR_QUOTE', commentString);
};

const sendAndUpdateTime = (roomId, commentStaring) => {
  updatePreviousAnnounceTime(roomId);
  sendToRoom(roomId, commentStaring);
};

function announceUsers(roomId) {
  const room = roomDatabase.getRoomById(roomId);
  if (room) {
    const { users } = room;
    const quote = getParticipantsQuote(users);
    sendAndUpdateTime(roomId, quote);
  }
}

function announceUsersProgress(roomId) {
  const { users } = roomDatabase.getRoomById(roomId);
  const toSend = getUsersProgressQuote(users);
  sendToRoom(roomId, toSend);
  updateScheduledAnnounceTime(roomId);
}

const checkTimeBetweenAnnounce = (previousAnnounce, nextAnnounce) => {
  const now = Date.now();
  return (now - previousAnnounce > 5000 && nextAnnounce - now > 5000);
};

function fillTimeBetweenAnnounce(roomId) {
  const { previousAnnounce, nextScheduledAnnounceTime, jokes } = getActiveCommentator(roomId);
  if (checkTimeBetweenAnnounce(previousAnnounce, nextScheduledAnnounceTime)) {
    sendAndUpdateTime(roomId, jokes);
  }
}

function userNearFinish(room, userId) {
  const { username } = room.users.find(user => user.id === userId);
  const { id: roomId } = room;
  const quoteNearFinish = new PhraseProxy('nearFinish').getText();
  sendAndUpdateTime(roomId, `${username} ${quoteNearFinish}!`);
}

function userFinished(room, userId) {
  const { username } = room.users.find(user => user.id === userId);
  const { id: roomId } = room;
  const quoteBeforeFinisher = new PhraseProxy('beforeFinisher').getText();
  sendAndUpdateTime(roomId, `${quoteBeforeFinisher} ${username}.`);
}

const filterFinishedUsers = users => users.filter(user => user.finished);

const sortFinishTimeDesc = (userA, userB) => userA.finished - userB.finished;

const mapUsersWithTime = (gameStartedAt, users) => (users
  .map(user => ([user.username, secondsSpentForRace(user.finished, gameStartedAt)]))
);

function gameOver(roomId) {
  const { gameStartedAt, users } = roomDatabase.getRoomById(roomId);
  const finishedUsers = filterFinishedUsers(users);
  const gameTimeIsOver = !!(finishedUsers.length < users.length);

  const sortedUsers = finishedUsers.sort(sortFinishTimeDesc);
  const topThreeUsers = mapUsersWithTime(gameStartedAt, sortedUsers).slice(0, 3);

  let finalQuote = '';

  switch (true) {
    case (!topThreeUsers.length): {
      finalQuote = new PhraseProxy('noOneWon').getText();
      break;
    }
    case (topThreeUsers.length === 1): {
      const [username, userTimeSpent] = topThreeUsers[0];
      const withResult = new PhraseProxy('withResult').getText();
      const onlyOneFinished = new PhraseProxy('onlyOneFinished').getText();
      finalQuote = `${onlyOneFinished} ${username} ${withResult} ${userTimeSpent}сек.`;
      break;
    }
    default: {
      const places = new PhraseProxy('places').getText();
      const usersPlaces = topThreeUsers.map((user, index) => {
        const [username, userTimeSpent] = user;
        const withResult = new PhraseProxy('withResult').getText();
        return `${places[index]} ${username} ${withResult} ${userTimeSpent}cек.`;
      });

      const { username: lastUsername } = sortedUsers[sortedUsers.length - 1];
      const timeIsOver = new PhraseProxy('timeIsOver').getText();
      const finalResult = new PhraseProxy('finalResult').getText();
      const beforeFinisher = new PhraseProxy('beforeFinisher').getText();
      const gameOverQuote = gameTimeIsOver
        ? `${timeIsOver}! ${finalResult}`
        : `${beforeFinisher} ${lastUsername}. ${finalResult}`;

      finalQuote = gameOverQuote + usersPlaces.join(' ');
    }
  }
  sendToRoom(roomId, finalQuote);
  deleteCommentator(roomId);
}

// factory
function progress(room, userId) {
  if (!room) {
    return;
  }

  const user = room.users.find(user => user.id === userId);
  const { currentCharacter, totalCharacters } = user?.progress;

  if (isUserNearFinish(currentCharacter, totalCharacters)) {
    userNearFinish(room, userId);
  }
  if (currentCharacter === totalCharacters) {
    userFinished(room, userId);
  }
}

const getRandFromArr = arr => arr[Math.floor(Math.random() * arr.length)];

const getRandomTypeOfJokes = () => getRandFromArr(quotes.typesOfJokes);

// eslint-disable-next-line
function commentator(roomId, socket, jokes) {
  setTimeout(announceUsers, 7000, roomId);
  const scheduledAnnounceProgress = setInterval(
    announceUsersProgress, (SECONDS_SCHEDULED_ANNOUNCE_INTERVAL * 1000), roomId
  );

  const now = Date.now();
  const jokesInterval = setInterval(
    fillTimeBetweenAnnounce, (SECONDS_SCHEDULED_JOKE_INTERVAL * 1000), roomId
  );

  addCommentator(roomId, {
    roomId,
    socket,
    jokes,
    jokesInterval,
    nextScheduledAnnounceTime: (now + (SECONDS_SCHEDULED_ANNOUNCE_INTERVAL * 1000)),
    scheduledAnnounceInterval: scheduledAnnounceProgress,
    previousAnnounce: now
  });

  const toSend = new PhraseProxy('startQuote').getText();
  sendToRoom(roomId, toSend);
}

// facade
function createCommentator(roomId, socket) {
  const type = getRandomTypeOfJokes();
  switch (type) {
    case 'programming': {
      const jokes = getRandFromArr(quotes.programmingJokes);
      commentator(roomId, socket, jokes);
      break;
    }
    case 'Ironic': {
      const jokes = getRandFromArr(quotes.ironicJokes);
      commentator(roomId, socket, jokes);
      break;
    }
    default: {
      const jokes = getRandFromArr(quotes.classicJokes);
      commentator(roomId, socket, jokes);
    }
  }
}

export default {
  createCommentator,
  deleteCommentator,
  gameOver,
  progress
};
