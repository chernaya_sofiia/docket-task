import { userLogin, userDisconnected } from '../services/userService';
import {
  userCreatedRoom,
  userJoinedRoom,
  userLeftRoom,
  updateRoom,
  updateRoomProgress
} from '../services/roomService';

export default io => {
  io.on('connection', socket => {
    socket.on('disconnecting', () => {
      const rooms = Object.keys(socket.rooms);
      rooms.forEach(roomId => {
        userLeftRoom(roomId, socket);
      });
    });
    socket.on('disconnect', () => userDisconnected(socket.id));

    const { handshake: { query: { username } } } = socket;
    userLogin(socket.id, username, socket);

    socket.on('JOIN_ROOM', roomId => userJoinedRoom(roomId, socket));
    socket.on('LEAVE_ROOM', roomId => userLeftRoom(roomId, socket));
    socket.on('CREATE_NEW_ROOM', roomId => userCreatedRoom(roomId, socket));
    socket.on('USER_READY', roomId => updateRoom(roomId, socket));
    socket.on('GAME_PROGRESS', ({ progress, roomId }) => updateRoomProgress(progress, roomId, socket));
  });
};
